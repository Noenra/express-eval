import { Application } from "express";
import HomeController from "./controllers/HomeController";
import ProjetController from "./controllers/ProjetController";

export default function route(app: Application)
{
    app.get("/", (req, res) =>
    {
        HomeController.home(req, res);   
    });

    app.get("/create", (req, res) =>
    {
        ProjetController.viewCreateForm(req, res);
    });

    app.post("/create", (req, res) =>
    {
        ProjetController.create(req, res);
    });

    app.get("/edit/:id", (req, res) =>
    {
        ProjetController.viewEditForm(req, res);
    });

    app.post("/edit/:id", (req, res) =>
    {
        ProjetController.edit(req, res);
    });

    app.get("/delete/:id", (req, res) =>
    {
        ProjetController.delete(req, res);
    });

    app.get("/projet/:id", (req, res) =>
    {
        ProjetController.viewProjet(req, res);
    });
}