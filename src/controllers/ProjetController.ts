import { Request, Response } from "express-serve-static-core";
import { db } from "../server";
import HomeController from "./HomeController";

export default class ProjetController
{
    static viewCreateForm(req: Request, res: Response): void
    {
        res.render('pages/create');
    }

    static create(req: Request, res: Response): void
    {
        const create = db.prepare(`INSERT INTO projets ("title", "content", "clients_id") VALUES (?, ?, ?)`).run(req.body.title, req.body.content, 1);
        HomeController.home(req, res);
    }

    static viewEditForm(req: Request, res: Response): void
    {
        const viewEdit = db.prepare("SELECT * FROM projets WHERE id=?").get(req.params.id);
        res.render('pages/edit', {
            projet: viewEdit,
        });
    }

    static edit(req: Request, res: Response): void
    {
        const edit = db.prepare('UPDATE projets SET title = ?, content = ? WHERE id = ?').run(req.body.title, req.body.content, req.params.id);
        HomeController.home(req, res);
    }

    static viewProjet(req: Request, res: Response): void
    {
        const proj = db.prepare("SELECT * FROM projets WHERE id=?").get(req.params.id);
        res.render('pages/projet', {
            projet: proj,
        });
    }

    static delete(req: Request, res: Response): void
    {
        db.prepare("DELETE FROM projets WHERE id=?").run(req.params.id);
        HomeController.home(req, res);
    }
}