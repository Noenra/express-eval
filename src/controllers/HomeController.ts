import { Request, Response } from "express-serve-static-core";
import { db } from "../server";

export default class HomeController
{
    static home(req: Request, res: Response): void
    {
        const projets = db.prepare("SELECT * FROM projets").all();
        res.render('pages/home', {
            proj: projets,
        });
    }
}